import csv
import glob
import sys
import json
import os
from utils import read_matches, read_players, read_player_history

def update_player_history(matches, players, player_history):

    for player in players:
        for match in matches:
            if match.player_was_in_match(player):
                player.play_match(match)
        if player.get_name() not in player_history:
            player_history[player.get_name()] = {'tables': set([int(t.get_table_number()) for t in player.get_tables()]), 
                                            'terrain_types': list([t.get_terrain_name() for t in player.get_tables()]),
                                            'player_name': player.get_name()}
        else:
            player_history[player.get_name()]['tables'].extend(set([int(t.get_table_number()) for t in player.get_tables()]))
            player_history[player.get_name()]['terrain_types'].extend([t.get_terrain_name() for t in player.get_tables()])
        
        player_history[player.get_name()]['tables'] = list(set(player_history[player.get_name()]['tables']))
        player_history[player.get_name()]['terrain_types'] = list(player_history[player.get_name()]['terrain_types'])
    
    return player_history

if __name__ == "__main__":
    input_match_file_name = str(sys.argv[1])
    
    for match_file in glob.glob('output/*final*'):
        player_history = read_player_history('output/player_history.json')
        matches = read_matches(match_file)
        players = read_players('input/players.csv')
        player_history = update_player_history(matches, players, player_history)

        # Write the player history to a json file
        with open('output/player_history.json', 'w',encoding='utf-8') as outfile:
            json.dump(player_history, outfile, indent=4, ensure_ascii=False)

        print("Read from " + match_file)

    