import random
import sys
from os.path import exists
from utils import read_matches, read_players, read_player_history, read_tables, write_matches
from Player import Player
from Table import Table
from Match import Match

def remove_contradicting_tables(match_tables, player_history):
    if not player_history:
        return match_tables

    possible_matches = match_tables

    for match in match_tables:
        for table in match_tables[match]:
            player1, player2 = match.get_players()

            player1_tables = player_history[player1.get_id()]['tables']
            player2_tables = player_history[player2.get_id()]['tables']
            
            if (table in player1_tables) or (table in player2_tables):
                possible_matches[match].remove(table)

    return possible_matches

def choose_suitable_table_for_match(table_numbers, match, table_dictionary, player_history):
    if not player_history:
        return random.choice(table_numbers)

    player1, player2 = match.get_players()

    player1_terrain_types = player_history[player1.get_id()]['terrain_types']
    player2_terrain_types = player_history[player2.get_id()]['terrain_types']

    for table_number in table_numbers:
        table = table_dictionary[table_number]
        terrain_type = table.get_terrain_name()
        if (terrain_type not in player1_terrain_types) and (terrain_type not in player2_terrain_types):
            return int(table.get_table_number())

    return random.choice(table_numbers)

    

def remove_table_from_matches(match_tables, table):
    possible_matches = match_tables

    for match in match_tables:
        if table in match_tables[match]:
            match_tables[match].remove(table)
    return possible_matches

def get_table_pairings(matches, tables, player_history):
    final_matches = []
    match_tables = {}

    for match in matches:
        match_tables[match] = list([int(t.get_table_number()) for t in tables])

    table_dict = {}

    for table in tables:
        table_dict[int(table.get_table_number())] = table

    # Remove tables that contradict player history
    # Assign matches to tables with the least number of possible tables
    # Remove table that has already been assigned

    while match_tables.items():
        match_tables = remove_contradicting_tables(match_tables, player_history)
        minimum_match = min(match_tables, key=lambda x: len(match_tables[x]))
        minimum_match_tables = match_tables.pop(minimum_match)
        chosen_table = choose_suitable_table_for_match(minimum_match_tables, minimum_match, table_dict, player_history)
        matches.remove(minimum_match)

        minimum_match.set_table(table_dict[chosen_table])
        final_matches.append(minimum_match)

        match_tables = remove_table_from_matches(match_tables, chosen_table)
    
    return final_matches

if __name__ == "__main__":
    match_file_name = str(sys.argv[1])
    matches = read_matches('input/' + match_file_name)
    player_history = read_player_history('output/player_history.json')
    tables = read_tables('input/tables.csv')

    matches = get_table_pairings(matches, tables, player_history)
    out_file_path = 'output/' + match_file_name.replace('raw', 'final')
    if not exists(out_file_path):
        write_matches(matches, out_file_path)
