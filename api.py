import json
from flask import Flask

app = Flask(__name__)

@app.route("/playerhistory/<tournament_id>/<round_id>", methods=['GET', 'POST'])
def hello_world(tournament_id, round_id):

    uri_path = 'tournamentstorage/' + tournament_id + '/round' + round_id + '/'
    player_history_file = open(uri_path + '/player_history.json')

    json_data = json.load(player_history_file)

    return json_data
