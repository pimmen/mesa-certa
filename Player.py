class Player:
    def __init__(self, name, id) -> None:
        self.name = name
        self.id = id
        self.match_history = set()

    def play_match(self, match):
        self.match_history.add(match)

    def get_name(self):
        return self.name
    
    def get_id(self):
        return self.id
    
    def get_tables(self):
        tables = set()
        for match in self.match_history:
            tables.add(match.get_table())
        return tables
