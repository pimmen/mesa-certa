from pairingalgorithm import get_table_pairings
from generate_player_history import update_player_history
from utils import read_matches, read_players, read_player_history, read_tables, write_matches, write_player_history
import sys
import glob

if __name__ == "__main__":
    for match_file in glob.glob('input/*matches_raw*'):
        player_history_file = glob.glob('output/*player_history*')
        player_history = {}
        if player_history_file:
            player_history = read_player_history(player_history_file[0])
        
        matches = read_matches(match_file)
        players = read_players('input/players.csv')
        tables = read_tables('input/tables.csv')

        matches = get_table_pairings(matches, tables, player_history)
        write_matches(matches, match_file.replace('raw', 'final'))

        player_history = update_player_history(matches, players, player_history)

        if player_history_file:
            write_player_history(player_history, player_history_file[0])
        else:
            write_player_history(player_history, 'output/player_history.json')
