import csv
import sys
from Player import Player
from Table import Table

def format_matches_file(matches_file_name, output_file_name):
    with open(matches_file_name, newline='',encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile)
        line_count = 0
        matches = []
        for row in reader:
            if line_count == 0:
                line_count += 1
                continue
            match = {}
            player_1 = Player(row[2], row[2])
            player_2 = Player(row[5], row[5])
            table = Table(row[1], row[1])
            match['player_1_name'] = player_1.get_name()
            match['player_1_id'] = player_1.get_id()
            match['player_2_name'] = player_2.get_name()
            match['player_2_id'] = player_2.get_id()
            match['table_number'] = table.get_table_number()
            matches.append(match)
            line_count += 1
        
        outfile = open(output_file_name, 'w', newline='',encoding='utf-8')
        writer = csv.DictWriter(outfile, fieldnames=['player_1_name', 'player_1_id', 'player_2_name', 'player_2_id', 'table_number'])
        writer.writeheader()
        writer.writerows(matches)
                   
if __name__=='__main__':
    input_match_file_name = str(sys.argv[1])
    output_match_file_name = str(sys.argv[2])
    format_matches_file('input/' + input_match_file_name, 'output/', output_match_file_name)
    print('done')
