import pandas as pd

tournament_df = pd.read_csv('input/Killzone Alphaspel V Pairings Export.csv')

unique_rounds = tournament_df['Round'].unique()

tournament_round_dict = {elem : pd.DataFrame for elem in unique_rounds}

for key in tournament_round_dict.keys():
    tournament_round_dict[key] = tournament_df[:][tournament_df['Round'] == key]

for key in tournament_round_dict:
    round = key
    output_df = pd.DataFrame(columns=['player_1_name', 'player_1_id', 'player_2_name', 'player_2_id', 'table_number'])
    output_df['player_1_name'] = tournament_round_dict[key]['Player 1']
    output_df['player_1_id'] = tournament_round_dict[key]['Player 1']
    output_df['player_2_name'] = tournament_round_dict[key]['Player 2']
    output_df['player_2_id'] = tournament_round_dict[key]['Player 2']
    output_df['table_number'] = tournament_round_dict[key]['Table']
    
    output_df.to_csv('output/round_' + str(round) + '_matches_raw.csv', index=False)

